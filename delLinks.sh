#!/bin/bash
Path="$(pwd)/";

publicLessJs="public/assets/frontend/desktop/less-js";
publicLess="public/assets/frontend/desktop/less";

resourceLessJs="node_modules/less/dist/";
resourceLess="resources/frontend/desktop/less/"

if [ -d $Path$publicLessJs ]; then
#    echo "less-js symLink exist";
    rm $publicLessJs;
    
    if [ -n $publicLessJs ]; then
        echo "less-js symLink removed";
    fi

    else
        echo "could not delete symLink less-js or symLink not exist";
fi

if [ -d $Path$publicLess ]; then
#    echo "Less resources symLink exist";
    rm $Path$publicLess;

    if [ -n $publicLess ]; then
        echo "Less resources symLink removed";
    fi

    else
        echo "could not delete symLink Less resources or symLink not exist";
fi

