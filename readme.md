**Apache should look on the public folder**

require installed **nodejs!**    
if u want develop custom bootstrap on less or building js need create symbolic links

for windows run:
```
links.bat
```
for linux run:
```
bash links.sh
```
for remove symbolic links

windows run:
```
delLinks.bat
```
linux run:
```
bash delLinks.sh
```

npm:
```
npm install
gulp build-new
```
browser-sync reload worked, configure block "config" in **gulpfile.js**
```
//config
var hostName = "http://yourSiteName/";
```
after configure run:
```
gulp host
```

see more gulp commands: 
```
gulp -T
```

