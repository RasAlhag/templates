<!doctype html>
<?php
	require "../autoload.php";

	$currentUrl = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	echo "<pre>";
	echo "current url:&nbsp".$currentUrl;
	echo "</pre>";

	$someClass = new  \SomeClass\SomeClass();
	$someClass->someMethod();

?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

	<!--	uncomment this for development Less -->
	<!--<link href="/assets/frontend/desktop/less/bootstrap/bootstrap.less" rel="stylesheet/less" type="text/css">-->
	<!--<link href="/assets/frontend/desktop/less/font-awesome/font-awesome.less" rel="stylesheet/less" type="text/css">-->
    <!---->
	<!--<script>-->
	<!--	less = {-->
	<!--		env: "development",-->
	<!--		async: false,-->
	<!--		fileAsync: false,-->
	<!--		poll: 1000,-->
	<!--		functions: {},-->
	<!--		dumpLineNumbers: "comments",-->
	<!--		relativeUrls: false,-->
	<!--		rootpath: ":/a.com/"-->
	<!--	};-->
	<!--</script>-->
	<!--<script src="/assets/frontend/desktop/less-js/less.min.js"  type="text/javascript"></script>-->

    <link rel="stylesheet" href="assets/frontend/desktop/css/frontend-desktop.min.css" type="text/css">
    <link rel="stylesheet" href="assets/frontend/desktop/css/style.css" type="text/css">

    <script src="assets/frontend/desktop/js/frontend-desktop.min.js" type="text/javascript"></script>
    <script src="assets/frontend/desktop/js/script.js" type="text/javascript"></script>

    <title><?=$currentUrl?></title>
</head>
<body>
    <div class="block grey-border">

        <p>for using bootstarp and jQuery now<br>
            run: <br>
            <strong>npm install</strong><br>
            <strong>gulp build-new</strong><br>
			create symbolic links <br>
			windows: <strong>links.bat</strong><br>
			linux: <strong>links.sh</strong> <br>
			remove symbolic links <br>
			windows: <strong>delLinks.bat</strong> <br>
			linux: <strong>delLinks.sh</strong>  <br>

        </p>

    </div>

</body>
</html>