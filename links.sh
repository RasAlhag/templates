#!/bin/bash
Path="$(pwd)/";

publicLessJs="public/assets/frontend/desktop/less-js";
publicLess="public/assets/frontend/desktop/less";

resourceLessJs="node_modules/less/dist/";
resourceLess="resources/frontend/desktop/less/";

 if [ -d  $publicLessJs ]; then
     echo "symLink less-js already exist";

     else
#        echo "create symLink for less-js";
        ln -s $Path$resourceLessJs $Path$publicLessJs;

        if [ -d  $publicLessJs ]; then
            echo "symLink for less-js created";

            else
                echo "could not create symLink less-js";
        fi
  fi

if [ -d $publicLess ]; then
    echo "symLink Less resources already exist";

    else
#        echo "create symLink for Less resources";
        ln -s $Path$resourceLess $Path$publicLess;

        if [ -d $publicLess ]; then
            echo "symLink for Less resources created";

            else
                echo "could not create symLink Less resources";
        fi
  fi

