<?php

$composerAutoload = __DIR__ . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

if (file_exists($composerAutoload)) {

    require $composerAutoload;

}

spl_autoload_register(function ($class){

    $class = str_replace("\\", DIRECTORY_SEPARATOR, $class);

    require __DIR__ . DIRECTORY_SEPARATOR . $class . ".php";

});
