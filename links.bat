@echo off
echo checking symLinks

IF EXIST .\public\assets\frontend\desktop\less (
    echo symLink Less resources already exist

    ) ELSE (
        mklink /J public\assets\frontend\desktop\less resources\frontend\desktop\less

        IF EXIST .\public\assets\frontend\desktop\less (
            echo symLink Less resources created!

        ) ELSE (
            echo could not created symLink for Less resources
        )
    )

IF EXIST .\public\assets\frontend\desktop\less-js (
    echo symLink less-js already exist

    ) ELSE (
        mklink /J public\assets\frontend\desktop\less-js node_modules\less\dist

        IF EXIST .\public\assets\frontend\desktop\less-js (
            echo symLink less-js created!

        ) ELSE (
            echo could not created symLink for less-js
        )
    )

echo press any key for exit
@pause