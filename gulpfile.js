//config
var hostName = "http://yourSiteName/";
var projectPath = "yourPath";

//static paths
var publicFrontDesktop = "./public/assets/frontend/desktop/";
var developFrontDesktop = "./resources/frontend/desktop/";
var publicFrontMobile = "./public/assets/frontend/desktop/";
var developFrontMobile = "./resources/frontend/mobile/";

//nodejs Paths
var nodePath = "./node_modules/";
var nodePathFontAwesomeFonts = nodePath+"font-awesome/fonts/";
var nodePathFontAwesomeLess = nodePath+"font-awesome/less/";
var nodePathBootstrapJs = nodePath+"bootstrap/js/";
var nodePathBootstrapFonts = nodePath+"bootstrap/fonts/";
var nodePathBootstrapLess = nodePath+"bootstrap/less/";
var nodePathJQuery = nodePath+"jquery/dist/";

//requires
var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var less = require('gulp-less');
var path = require('path');
var LessPluginCleanCSS = require('less-plugin-clean-css'),
	cleancss = new LessPluginCleanCSS({advanced: true});
var minifyCSS = require('gulp-minify-css');
var LessPluginAutoPrefix = require('less-plugin-autoprefix'),
	autoprefix = new LessPluginAutoPrefix({browsers: ["last 2 versions"]});
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var minify = require('gulp-minify');
var browserify = require('gulp-browserify');
var order = require("gulp-order");
var replace = require('gulp-replace');

//tasks

gulp.task('host', function () {
	browserSync.init({
		proxy: hostName
	});
	// ---- host ----
	gulp.watch("./*.php").on('change', browserSync.reload);
	// ---- end ----
});

gulp.task('bootstrap-install', function () {
	return gulp.src([
		nodePathBootstrapLess+'/**/*'
	])
		.pipe(gulp.dest(developFrontDesktop+'less/bootstrap'))
		.pipe(gulp.dest(developFrontMobile+'less/bootstrap'));
});

gulp.task('update-fonts', function () {
	return gulp.src([
			nodePathFontAwesomeFonts+'fontawesome-webfont.*',
			nodePathBootstrapFonts+'glyphicons-halflings-regular.*'
		])
		.pipe(gulp.dest(publicFrontDesktop+'fonts'))
		.pipe(gulp.dest(developFrontDesktop+'less/fonts'))

		.pipe(gulp.dest(publicFrontMobile+'fonts'))
		.pipe(gulp.dest(developFrontMobile+'less/fonts'));
});

gulp.task('update-font-awesome', function () {
	return gulp.src([
			nodePathFontAwesomeLess+'*.less'
		])
		.pipe(gulp.dest(developFrontDesktop+'less/font-awesome'))
		.pipe(gulp.dest(developFrontMobile+'less/font-awesome'));
});

gulp.task('update', ['update-fonts', 'update-font-awesome']);

gulp.task('less-desktop', function () {
	return gulp.src([
			developFrontDesktop+'less/bootstrap/bootstrap.less',
			developFrontDesktop+'less/font-awesome/font-awesome.less'
		])
		.pipe(less({
			//paths: [ path.join(__dirname, 'less', 'includes') ],
			plugins: [autoprefix, cleancss]//,
			//compress: true
		}))
		.pipe(concat('frontend-desktop.css'))
		.pipe(minifyCSS())
		.pipe(rename({suffix: ".min"}))
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(publicFrontDesktop+'css'));
});

gulp.task('js-desktop', function () {
	return gulp.src([
			//jquery
			nodePathJQuery+'jquery.min.js',
			//bootstrap
			nodePathBootstrapJs+'transition.js',
			nodePathBootstrapJs+'alert.js',
			nodePathBootstrapJs+'button.js',
			nodePathBootstrapJs+'carousel.js',
			nodePathBootstrapJs+'collapse.js',
			nodePathBootstrapJs+'dropdown.js',
			nodePathBootstrapJs+'modal.js',
			nodePathBootstrapJs+'tooltip.js',
			nodePathBootstrapJs+'popover.js',
			nodePathBootstrapJs+'scrollspy.js',
			nodePathBootstrapJs+'tab.js',
			nodePathBootstrapJs+'affix.js',
			//custom js
			developFrontDesktop+'js-dev/*.js'
		])
		.pipe(minify({
			noSource: true
		}))
		.pipe(concat('frontend-desktop.js'))
		.pipe(rename({suffix: ".min"}))
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(publicFrontDesktop+'js'));
});

gulp.task('build-desktop', ['less-desktop', 'js-desktop']);

gulp.task('watch', function () {
	gulp.watch([
			developFrontDesktop+'js-dev/*.js'
		],

		[
			'js-desktop'
		]);
});

gulp.task('less-mobile', function () {
	return gulp.src([
			developFrontMobile+'less/bootstrap/bootstrap.less',
			developFrontMobile+'less/font-awesome/font-awesome.less'
		])
		.pipe(less({
			//paths: [ path.join(__dirname, 'less', 'includes') ],
			plugins: [autoprefix, cleancss],
			//compress: true
		}))
		.pipe(concat('frontend-mobile.css'))
		.pipe(minifyCSS())
		.pipe(rename({suffix: ".min"}))
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(publicFrontMobile+'css'));
});

gulp.task('js-mobile', function () {
	return gulp.src([
			//jquery
			nodePathJQuery+'jquery.min.js',
			//bootstrap
			nodePathBootstrapJs+'transition.js',
			nodePathBootstrapJs+'alert.js',
			nodePathBootstrapJs+'button.js',
			nodePathBootstrapJs+'carousel.js',
			nodePathBootstrapJs+'collapse.js',
			nodePathBootstrapJs+'dropdown.js',
			nodePathBootstrapJs+'modal.js',
			nodePathBootstrapJs+'tooltip.js',
			nodePathBootstrapJs+'popover.js',
			nodePathBootstrapJs+'scrollspy.js',
			nodePathBootstrapJs+'tab.js',
			nodePathBootstrapJs+'affix.js',
			//custom js
			developFrontMobile+'js-dev/*.js'
		])
		.pipe(minify({
			noSource: true
		}))
		.pipe(concat('frontend-mobile.js'))
		.pipe(rename({suffix: ".min"}))
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(publicFrontMobile+'js'));
});

gulp.task('build-mobile', ['less-mobile', 'js-mobile']);

gulp.task('build-new', ['update', 'bootstrap-install', 'build-desktop']);

//gulp.task('default', ['server-zsk']);
