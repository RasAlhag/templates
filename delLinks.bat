@echo off
echo checking symLinks

IF EXIST .\public\assets\frontend\desktop\less (
    rmdir .\public\assets\frontend\desktop\less /Q

    IF NOT EXIST .\public\assets\frontend\desktop\less (
        echo symLink Less resources removed!
    )

) ELSE (
       echo symLink Less resources not exist
)

IF EXIST .\public\assets\frontend\desktop\less-js (
    rmdir .\public\assets\frontend\desktop\less-js /Q

    IF NOT EXIST .\public\assets\frontend\desktop\less-js (
        echo symLink less-js removed!
    )

) ELSE (
       echo symLink less-js not exist
)

echo press any key for exit
@pause